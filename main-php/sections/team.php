<section class="team-section overlay" id="team" style="background-image:url('assets/images/bg/team.jpg');">
    <div class="container">
        <div class="title-section">
            <div class="sec-title">
                <h2>
                    Our Team
                </h2>
            </div>
            <div class="info">
                <p>
                    Nullam sit amet odio eu est aliquet euismod a a urna. Proin eu urna suscipit, dictum quam nec.
                </p>
            </div>
            <span class="horizontal-line"></span>
        </div>
        <div class="team-inner">
            <div class="team-slider">
                <div>
                    <div class="team-member">
                        <div class="image">
                            <img src="assets/images/slider/team/team1.jpg" alt="">
                        </div>
                        <div class="team-info">
                            <div class="name">
                                <h4>john doe <span>web developer</span></h4>
                            </div>
                            <div class="social">
                                <a href=""><i class="fa fa-facebook"></i></a>
                                <a href=""><i class="fa fa-twitter"></i></a>
                                <a href=""><i class="fa fa-youtube"></i></a>
                                <a href=""><i class="fa fa-linkedin"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="team-member">
                        <div class="image">
                            <img src="assets/images/slider/team/team1.jpg" alt="">
                        </div>
                        <div class="team-info">
                            <div class="name">
                                <h4>john doe <span>web developer</span></h4>
                            </div>
                            <div class="social">
                                <a href=""><i class="fa fa-facebook"></i></a>
                                <a href=""><i class="fa fa-twitter"></i></a>
                                <a href=""><i class="fa fa-youtube"></i></a>
                                <a href=""><i class="fa fa-linkedin"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="team-member">
                        <div class="image">
                            <img src="assets/images/slider/team/team1.jpg" alt="">
                        </div>
                        <div class="team-info">
                            <div class="name">
                                <h4>john doe <span>web developer</span></h4>
                            </div>
                            <div class="social">
                                <a href=""><i class="fa fa-facebook"></i></a>
                                <a href=""><i class="fa fa-twitter"></i></a>
                                <a href=""><i class="fa fa-youtube"></i></a>
                                <a href=""><i class="fa fa-linkedin"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="team-member">
                        <div class="image">
                            <img src="assets/images/slider/team/team1.jpg" alt="">
                        </div>
                        <div class="team-info">
                            <div class="name">
                                <h4>john doe <span>web developer</span></h4>
                            </div>
                            <div class="social">
                                <a href=""><i class="fa fa-facebook"></i></a>
                                <a href=""><i class="fa fa-twitter"></i></a>
                                <a href=""><i class="fa fa-youtube"></i></a>
                                <a href=""><i class="fa fa-linkedin"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="team-member">
                        <div class="image">
                            <img src="assets/images/slider/team/team1.jpg" alt="">
                        </div>
                        <div class="team-info">
                            <div class="name">
                                <h4>john doe <span>web developer</span></h4>
                            </div>
                            <div class="social">
                                <a href=""><i class="fa fa-facebook"></i></a>
                                <a href=""><i class="fa fa-twitter"></i></a>
                                <a href=""><i class="fa fa-youtube"></i></a>
                                <a href=""><i class="fa fa-linkedin"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>