<section class="case-section">
    <div class="container">
        <div class="title-section">
            <div class="sec-title">
                <h2>
                    CASE STUDY
                </h2>
            </div>
            <div class="info">
                <p>
                    A brief story about how this process works, keep an eye till the end.s
                </p>
            </div>
            <span class="horizontal-line alt"></span>
        </div>
        <div class="case-study">
            <div class="case-info">
                <div class="case-slider">
                    <div>
                        <div class="cases">
                            <div class="image">
                                <span class="square"></span>
                                <img src="assets/images/icons/bulb.png" alt="case-study">
                            </div>
                            <div class="title alt line">
                                <h4>
                                    ACCUMULATE CREATIVE IDEAS
                                </h4>
                            </div>
                            <div class="info">
                                <p>
                                    Lorem Ipsum is simply dummy text of the printing and typesetting let. Lorem Ipsum
                                    has
                                    been
                                    the
                                    industry. Lorem Ipsum is simply dummy text of the printing and typesetting let.
                                    Lorem
                                    Ipsum
                                    has
                                    been the industry Printing and typelorem Ipsum has been the setting let.
                                </p>
                            </div>
                            <div class="button-group">
                                <a href="#" class="c-btn c-primary-btn c-btn-md ">Read More</a>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="cases">
                            <div class="image">
                                <span class="square"></span>
                                <img src="assets/images/icons/bulb.png" alt="case-study">
                            </div>
                            <div class="title alt line">
                                <h4>
                                    ACCUMULATE CREATIVE IDEAS
                                </h4>
                            </div>
                            <div class="info">
                                <p>
                                    Lorem Ipsum is simply dummy text of the printing and typesetting let. Lorem Ipsum
                                    has
                                    been
                                    the
                                    industry. Lorem Ipsum is simply dummy text of the printing and typesetting let.
                                    Lorem
                                    Ipsum
                                    has
                                    been the industry Printing and typelorem Ipsum has been the setting let.
                                </p>
                            </div>
                            <div class="button-group">
                                <a href="#" class="c-btn c-primary-btn c-btn-md ">Read More</a>
                            </div>
                        </div>
                    </div>

                    <div>
                        <div class="cases">
                            <div class="image">
                                <span class="square"></span>
                                <img src="assets/images/icons/bulb.png" alt="case-study">
                            </div>
                            <div class="title alt line">
                                <h4>
                                    ACCUMULATE CREATIVE IDEAS
                                </h4>
                            </div>
                            <div class="info">
                                <p>
                                    Lorem Ipsum is simply dummy text of the printing and typesetting let. Lorem Ipsum
                                    has
                                    been
                                    the
                                    industry. Lorem Ipsum is simply dummy text of the printing and typesetting let.
                                    Lorem
                                    Ipsum
                                    has
                                    been the industry Printing and typelorem Ipsum has been the setting let.
                                </p>
                            </div>
                            <div class="button-group">
                                <a href="#" class="c-btn c-primary-btn c-btn-md ">Read More</a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="case-image">
                <div class="case-for">
                    <img src="assets/images/slider/case/case1.jpg" alt="case-study">
                    <img src="assets/images/slider/case/case2.jpg" alt="case-study">
                    <img src="assets/images/slider/case/case3.jpg" alt="case-study">
                </div>

            </div>
        </div>
    </div>
</section>