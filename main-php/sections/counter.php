<section class="counter-section overlay" style="background-image:url('assets/images/bg/bg1.jpg');">
    <div class="container">
        <div class="counter-slider">
            <div>
                <div class="counter">
                    <div class="image">
                        <div class="circle">
                            <img src="assets/images/icons/heart.png" alt="">
                        </div>
                    </div>
                    <div class="hits">
                        <span>
                            3891 
                        </span>
                    </div>
                    <div class="title">
                        <h4>
                            User Favourites
                        </h4>
                    </div>
                </div>
            </div>
            <div>
                <div class="counter">
                    <div class="image">
                        <div class="circle">
                            <img src="assets/images/icons/heart.png" alt="">
                        </div>
                    </div>
                    <div class="hits">
                        <span>
                            3891 
                        </span>
                    </div>
                    <div class="title">
                        <h4>
                            User Favourites
                        </h4>
                    </div>
                </div>
            </div>
            <div>
                <div class="counter">
                    <div class="image">
                        <div class="circle">
                            <img src="assets/images/icons/heart.png" alt="">
                        </div>
                    </div>
                    <div class="hits">
                        <span>
                            3891 
                        </span>
                    </div>
                    <div class="title">
                        <h4>
                            User Favourites
                        </h4>
                    </div>
                </div>
            </div>
            <div>
                <div class="counter">
                    <div class="image">
                        <div class="circle">
                            <img src="assets/images/icons/heart.png" alt="">
                        </div>
                    </div>
                    <div class="hits">
                        <span>
                            3891 
                        </span>
                    </div>
                    <div class="title">
                        <h4>
                            User Favourites
                        </h4>
                    </div>
                </div>
            </div>
            <div>
                <div class="counter">
                    <div class="image">
                        <div class="circle">
                            <img src="assets/images/icons/heart.png" alt="">
                        </div>
                    </div>
                    <div class="hits">
                        <span>
                            3891 
                        </span>
                    </div>
                    <div class="title">
                        <h4>
                            User Favourites
                        </h4>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</section>