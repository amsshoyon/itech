function require(jspath) {
    document.write('<script type="text/javascript" src="' + jspath + '"><\/script>');
}

require("assets/vendor/jquery/jquery.min.js");
require("assets/vendor/popper/popper.min.js");
require("assets/vendor/bootstrap/dist/js/bootstrap.min.js");
require("assets/vendor/bootstrap/dist/js/bootstrap-swipe-carousel.min.js");
require("assets/vendor/slick/slick.min.js");
require("assets/vendor/isotope/isotope.pkgd.min.js");
require("assets/js/custom.js");

