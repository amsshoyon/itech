$(document).ready(function () {
    //Document ready function start
    //-----------------------------------------------------------------------

    // Home Slider Carousel
    //====================================================
    $('#home-slider').carousel().swipeCarousel({
        // options here
    });

    // Service Slider
    //====================================================
    $('.services').slick({
        dots: true,
        arrows: false,
        vertical: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        verticalSwiping: true,
        asNavFor: '.slider-for',
        focusOnSelect: true,
        autoplay: true,
        responsive: [
            {
                breakpoint: 769,
                settings: {
                    slidesToShow: 1,
                    vertical: false,
                    verticalSwiping: false,

                }
            }
        ]
    });
    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.services',
        lazyLoad: 'ondemand',
        swipe: false,
    }).on('setPosition', function (event, slick) {
        slick.$slides.css('height', slick.$slideTrack.height() + 'px');
    });

    // Gallary
    //====================================================
    $('.grid').isotope({
        itemSelector: '.grid-item',
    });

    // filter items on button click
    $('.filters').on('click', 'li', function () {
        var filterValue = $(this).attr('data-filter');
        $('.grid').isotope({ filter: filterValue });
        $('.filters li').removeClass('active');
        $(this).addClass('active');
    });

    // Case Slider
    //==========================================================
    $('.case-slider').slick({
        dots: true,
        arrows: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        asNavFor: '.case-for',
        focusOnSelect: true,
        autoplay: false,
        responsive: [
            {
                breakpoint: 769,
                settings: {
                    slidesToShow: 1,
                    vertical: false,
                    verticalSwiping: false,

                }
            }
        ]
    });
    $('.case-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.case-slider',
        lazyLoad: 'ondemand',
        swipe: false,

    }).on('setPosition', function (event, slick) {
        slick.$slides.css('height', slick.$slideTrack.height() + 'px');
    });



    // Counter Slider
    //==========================================================
    $('.counter-slider').slick({
        dots: false,
        arrows: false,
        slidesToShow: 5,
        slidesToScroll: 1,
        autoplay: false,
        responsive: [
            {
                breakpoint: 769,
                settings: {
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 481,
                settings: {
                    slidesToShow: 1,
                    dots: true,
                }
            }
        ]
    });

    // Pricing Slider
    //==========================================================
    $('.pricing-slider').slick({
        dots: false,
        arrows: false,
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: false,
        responsive: [
            {
                breakpoint: 769,
                settings: {
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 481,
                settings: {
                    slidesToShow: 1,
                    dots: true,
                }
            }
        ]
    });

    // Client Slider
    //==========================================================
    $('.client-slider').slick({
        dots: false,
        arrows: false,
        slidesPerRow: 5,
        rows: 2,
        responsive: [
            {
                breakpoint: 1168,
                settings: {
                    slidesPerRow: 4,
                    dots: true,
                }
            },
            {
                breakpoint: 981,
                settings: {
                    slidesPerRow: 3,
                    dots: true,
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesPerRow: 2,
                    dots: true,
                }
            }
        ]
    });

    // Team Slider
    //==========================================================
    $('.team-slider').slick({
        dots: true,
        arrows: false,
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplay: false,
        responsive: [
            {
                breakpoint: 1025,
                settings: {
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 769,
                settings: {
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 481,
                settings: {
                    slidesToShow: 1,
                    dots: true,
                }
            }
        ]
    });


    // Contact form
    //==========================================================

    $('.input').focus(function () {
        $(this).parent().find(".label-text").addClass('label-active');
    });

    $(".input").focusout(function () {
        if ($(this).val() == '') {
            $(this).parent().find(".label-text").removeClass('label-active');
        };
    });

    // Go Top
    //==========================================================

    $(window).scroll(function () {
        if ($(this).scrollTop() > 500) {
            document.getElementById("go-top").setAttribute("style", "display:flex");
        } else {
            document.getElementById("go-top").setAttribute("style", "display:none");
        }
    });
    //-----------------------------------------------------------------------
    //Document ready function end
});