<section class="blog-section" id="blogs">
    <div class="title-section">
        <div class="sec-title">
            <h2>
                OUR BLOG
            </h2>
        </div>
        <div class="info">
            <p>
                Suspendisse sed eros mollis, tincidunt felis eget, interdum eratullam sit amet odio.
            </p>
        </div>
        <span class="horizontal-line alt"></span>
    </div>
    <div class="blog-inner">
        <div class="blog">
            <div class="image">
                <img src="assets/images/slider/blog/blog1.jpg" alt="">
            </div>
            <div class="blog-desp">
                <div class="category">
                    <p>
                        art / t-shirt
                    </p>
                </div>
                <div class="title">
                    <h3>
                        T-SHIRT DESIGN
                    </h3>
                </div>
                <div class="info">
                    <p>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                        the indu Stry's standard dummy text ever since the 1500s, an unknown printer took a galley of
                        type a scrambled it to make a type specimen book.
                    </p>
                </div>
                <div class="read-more">
                    <a href="#" class="read-more-btn">Read More <i class="fa  fa-angle-double-right"></i></a>
                </div>
            </div>
        </div>
        <div class="blog">
            <div class="image">
                <img src="assets/images/slider/blog/blog2.jpg" alt="">
            </div>
            <div class="blog-desp">
                <div class="category">
                    <p>
                        art / t-shirt
                    </p>
                </div>
                <div class="title">
                    <h3>
                        T-SHIRT DESIGN
                    </h3>
                </div>
                <div class="info">
                    <p>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                        the indu Stry's standard dummy text ever since the 1500s, an unknown printer took a galley of
                        type a scrambled it to make a type specimen book.
                    </p>
                </div>
                <div class="read-more">
                    <a href="#" class="read-more-btn">Read More <i class="fa  fa-angle-double-right"></i></a>
                </div>
            </div>
        </div>
        <div class="blog">
            <div class="image">
                <img src="assets/images/slider/blog/blog3.jpg" alt="">
            </div>
            <div class="blog-desp">
                <div class="category">
                    <p>
                        art / t-shirt
                    </p>
                </div>
                <div class="title">
                    <h3>
                        T-SHIRT DESIGN
                    </h3>
                </div>
                <div class="info">
                    <p>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                        the indu Stry's standard dummy text ever since the 1500s, an unknown printer took a galley of
                        type a scrambled it to make a type specimen book.
                    </p>
                </div>
                <div class="read-more">
                    <a href="#" class="read-more-btn">Read More <i class="fa  fa-angle-double-right"></i></a>
                </div>
            </div>
        </div>
        <div class="blog">
            <div class="image">
                <img src="assets/images/slider/blog/blog4.jpg" alt="">
            </div>
            <div class="blog-desp">
                <div class="category">
                    <p>
                        art / t-shirt
                    </p>
                </div>
                <div class="title">
                    <h3>
                        T-SHIRT DESIGN
                    </h3>
                </div>
                <div class="info">
                    <p>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                        the indu Stry's standard dummy text ever since the 1500s, an unknown printer took a galley of
                        type a scrambled it to make a type specimen book.
                    </p>
                </div>
                <div class="read-more">
                    <a href="#" class="read-more-btn">Read More <i class="fa  fa-angle-double-right"></i></a>
                </div>
            </div>
        </div>


    </div>
</section>