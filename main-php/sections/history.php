<section class="history-section" id="about">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-12">
                <div class="image">
                    <img src="assets/images/Monitor.png" alt="History">
                </div>
            </div>
            <div class="col-lg-6 col-md-12">
                <div class="history">
                    <div class="sec-title vertical-line">
                        <h2>
                            OUR HISTORY
                        </h2>
                    </div>
                    <div class="horizontal-line alt"></div>
                    <div class="info">
                        <p>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ip sum has
                            been the industry's standard dummy text ever since the 1500s, when an unk- nown printer took
                            a galley of type and scrambled it to make a type specimen book.
                        </p>
                        <p>
                            It has survived not only five centuries, but also the leap into electronic typesetting,
                            remaining essentially unchanged. It was popularised in the 1960s with the release of
                            Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing
                            software like Aldus PageMaker including versions of Lorem Ipsum.
                        </p>
                    </div>
                    <div class="button-group">
                        <a href="#" class="c-btn c-btn-lg c-primary-btn">BROWSE OUR WORK</a>
                    </div>
                </div>
            </div>


        </div>
    </div>
</section>