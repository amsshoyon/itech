<section class="gallary-section" id="gallary">
    <div class="title-section">
        <div class="sec-title">
            <h2>
                RECENT WORKS
            </h2>
        </div>
        <div class="info">
            <p>
                It has survived not only five centuries, but also the leap scrambled it to make a type.
            </p>
        </div>
        <span class="horizontal-line alt"></span>
    </div>

    <div class="filters">
        <ul>
            <li class="active" data-filter="*">All</li>
            <li data-filter=".print_design">PRINT DESIGN</li>
            <li data-filter=".animation">ANIMATION</li>
            <li data-filter=".art">ART</li>
            <li data-filter=".web_design">WEB DESIGN</li>
            <li data-filter=".photography">PHOTOGRAPHY</li>
            <li data-filter=".video">VIDEO</li>
        </ul>
    </div>

    <div class="grid">
        <div class="grid-item print_design animation">
            <div class="gallary-inner">
                <img class="p2" src="assets/images/gallary/1.jpg">
                <div class="gallary-info">
                    <div class="info-inner">
                        <div class="title line">
                            <h3>
                                T-SHIRT DESIGN
                            </h3>
                        </div>
                        <div class="desp">
                            <p>
                                Art / T-shirt
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="grid-item animation art">
            <div class="gallary-inner">
                <img class="p2" src="assets/images/gallary/2.jpg">
                <div class="gallary-info">
                    <div class="info-inner">
                        <div class="title line">
                            <h3>
                                T-SHIRT DESIGN
                            </h3>
                        </div>
                        <div class="desp">
                            <p>
                                Art / T-shirt
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="grid-item art web_design">
            <div class="gallary-inner">
                <img class="p2" src="assets/images/gallary/3.jpg">
                <div class="gallary-info">
                    <div class="info-inner">
                        <div class="title line">
                            <h3>
                                T-SHIRT DESIGN
                            </h3>
                        </div>
                        <div class="desp">
                            <p>
                                Art / T-shirt
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="grid-item web_design photography">
            <div class="gallary-inner">
                <img class="p2" src="assets/images/gallary/4.jpg">
                <div class="gallary-info">
                    <div class="info-inner">
                        <div class="title line">
                            <h3>
                                T-SHIRT DESIGN
                            </h3>
                        </div>
                        <div class="desp">
                            <p>
                                Art / T-shirt
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="grid-item photography video">
            <div class="gallary-inner">
                <img class="p2" src="assets/images/gallary/5.jpg">
                <div class="gallary-info">
                    <div class="info-inner">
                        <div class="title line">
                            <h3>
                                T-SHIRT DESIGN
                            </h3>
                        </div>
                        <div class="desp">
                            <p>
                                Art / T-shirt
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="grid-item video print_design">
            <div class="gallary-inner">
                <img class="p2" src="assets/images/gallary/6.jpg">
                <div class="gallary-info">
                    <div class="info-inner">
                        <div class="title line">
                            <h3>
                                T-SHIRT DESIGN
                            </h3>
                        </div>
                        <div class="desp">
                            <p>
                                Art / T-shirt
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="grid-item print_design photography">
            <div class="gallary-inner">
                <img class="p2" src="assets/images/gallary/7.jpg">
                <div class="gallary-info">
                    <div class="info-inner">
                        <div class="title line">
                            <h3>
                                T-SHIRT DESIGN
                            </h3>
                        </div>
                        <div class="desp">
                            <p>
                                Art / T-shirt
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="grid-item photography art">
            <div class="gallary-inner">
                <img class="p2" src="assets/images/gallary/8.jpg">
                <div class="gallary-info">
                    <div class="info-inner">
                        <div class="title line">
                            <h3>
                                T-SHIRT DESIGN
                            </h3>
                        </div>
                        <div class="desp">
                            <p>
                                Art / T-shirt
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
</section>