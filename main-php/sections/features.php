<section class="feature-section" id="features">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 d-flex justify-content-center align-items-center">
                <div class="feature">
                    <div class="image">
                        <span class="square"></span>
                        <img src="assets/images/icons/glass.png" alt="SLEEK DESIGN">
                    </div>
                    <div class="title bottom-line">
                        <h4>
                            SLEEK DESIGN
                        </h4>
                    </div>
                    <div class="info">
                        <p>
                            Lorem Ipsum is simply dummy text of the printing and typesetting let. Lorem Ipsum has been
                            the industry.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 d-flex justify-content-center align-items-center">
                <div class="feature">
                    <div class="image">
                        <span class="square"></span>
                        <img src="assets/images/icons/heart.png" alt=" CLEAN CODE">
                    </div>
                    <div class="title bottom-line">
                        <h4>
                            CLEAN CODE
                        </h4>
                    </div>
                    <div class="info">
                        <p>
                            Lorem Ipsum is simply dummy text of the printing and typesetting let. Lorem Ipsum has been
                            the industry.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 d-flex justify-content-center align-items-center">
                <div class="feature">
                    <div class="image">
                        <span class="square"></span>
                        <img src="assets/images/icons/bulb.png" alt="CREATIVE IDEAS">
                    </div>
                    <div class="title bottom-line">
                        <h4>
                            CREATIVE IDEAS
                        </h4>
                    </div>
                    <div class="info">
                        <p>
                            Lorem Ipsum is simply dummy text of the printing and typesetting let. Lorem Ipsum has been
                            the industry.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 d-flex justify-content-center align-items-center">
                <div class="feature">
                    <div class="image">
                        <span class="square"></span>
                        <img src="assets/images/icons/chat.png" alt=" FREE SUPPORT">
                    </div>
                    <div class="title bottom-line">
                        <h4>
                            FREE SUPPORT
                        </h4>
                    </div>
                    <div class="info">
                        <p>
                            Lorem Ipsum is simply dummy text of the printing and typesetting let. Lorem Ipsum has been
                            the industry.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>