<section class="service-section" id="services">
    <div class="services-inner">
        <div class="sec-title vertical-line alt">
            <h2>
                OUR SERVICES
            </h2>
        </div>
        <div class="horizontal-line"></div>
        <div class="slider-section alt">
            <div class="services">
                <div>
                    <div class="single-service">
                        <div class="desp">
                            <div class="title">
                                <h4>
                                    WEB DESIGN
                                </h4>
                            </div>
                            <div class="info">
                                <p>
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ip
                                    sum
                                    has
                                    been
                                    the industry's standard dummy text ever.
                                </p>
                            </div>
                        </div>
                        <div class="image">
                            <div class="circle">
                                <img src="assets/images/icons/print.svg" alt="WEB DESIGN">
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="single-service">
                        <div class="desp">
                            <div class="title">
                                <h4>
                                    PRINT DESIGN
                                </h4>
                            </div>
                            <div class="info">
                                <p>
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ip
                                    sum
                                    has
                                    been
                                    the industry's standard dummy text ever.
                                </p>
                            </div>
                        </div>
                        <div class="image">
                            <div class="circle">
                                <img src="assets/images/icons/monitor.svg" alt="WEB DESIGN">
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="single-service">
                        <div class="desp">
                            <div class="title">
                                <h4>
                                    PHOTOGRAPHY
                                </h4>
                            </div>
                            <div class="info">
                                <p>
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ip
                                    sum
                                    has
                                    been
                                    the industry's standard dummy text ever.
                                </p>
                            </div>
                        </div>
                        <div class="image">
                            <div class="circle">
                                <img src="assets/images/icons/monitor.svg" alt="WEB DESIGN">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="for-services">
        <div class="slider-for">
            <div class="image">
                <img src="assets/images/slider/services/slider1.jpg" alt="services">
            </div>
            <div class="image">
                <img src="assets/images/slider/services/slider2.jpg" alt="services">
            </div>
            <div class="image">
                <img src="assets/images/slider/services/slider3.jpg" alt="services">
            </div>
        </div>
</section>