<?php
include 'sections/head.php';
include 'sections/header.php';
include 'sections/home_banner.php';
include 'sections/features.php';
include 'sections/history.php';
include 'sections/services.php';
include 'sections/gallary.php';
include 'sections/case.php';
include 'sections/counter.php';
include 'sections/pricing.php';
include 'sections/team.php';
include 'sections/clients.php';
include 'sections/blog.php';
include 'sections/contact.php';
include 'sections/footer.php';
include 'sections/tail.php';