<section class="contact-section" id="contact">
    <div class="container">
        <div class="title-section">
            <div class="sec-title">
                <h2>
                    KEEP IN TOUCH
                </h2>
            </div>
            <div class="info">
                <p>
                    Nullam sit amet odio eu est aliquet euismod a a urna. Proin eu urna suscipit, dictum quam nec.
                </p>
            </div>
            <span class="horizontal-line alt"></span>
        </div>
        <div class="contact-inner">
            <div class="contact-address">
                <div class="address">
                    <div class="title">
                        <h3>
                            OUR ADDRESS
                        </h3>
                    </div>
                    <div class="info">
                        <h4>
                            House #13, Streat road, Sydney
                            2310 Australia
                        </h4>
                    </div>
                </div>
                <div class="address">
                    <div class="title">
                        <h3>
                            CALL US
                        </h3>
                    </div>
                    <div class="info">
                        <h4>
                            + 880 168 109 1425
                        </h4>
                        <h4>
                            + 0216809142
                        </h4>
                    </div>
                </div>
                <div class="address">
                    <div class="title">
                        <h3>
                            EMAIL US
                        </h3>
                    </div>
                    <div class="info">
                        <h4>
                            amsshoyon@gmail.com
                        </h4>
                    </div>
                </div>
            </div>
            <div class="contact-form">
                <form action="#">
                    <div class="form-group">
                        <label for="name" class="label-text">Name</label>
                        <input type="text" name="name" id="name" class="form-control input">
                    </div>
                    <div class="form-group">
                        <label for="email" class="label-text">Email</label>
                        <input type="email" name="email" id="email" class="form-control input">
                    </div>
                    <div class="form-group">
                        <label for="subject" class="label-text">Subject</label>
                        <input type="text" name="subject" id="subject" class="form-control input">
                    </div>
                    <div class="form-group">
                        <label for="message" class="label-text">Message</label>
                        <textarea name="message" id="message" class="form-control input" rows="6"></textarea>
                    </div>
                    <input type="submit" name="submit" class="c-btn c-btn-md c-primary-btn" value="Send Message">
                </form>
            </div>
        </div>
    </div>
</section>