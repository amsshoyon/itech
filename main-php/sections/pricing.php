<section class="pricing-section" id="pricing">
    <div class="container">
        <div class="title-section">
            <div class="sec-title">
                <h2>
                    OUR PRICING
                </h2>
            </div>
            <div class="info">
                <p>
                    A 30 days free trial for all. A brief story about how this process works, keep an eye till the end.
                </p>
            </div>
            <span class="horizontal-line alt"></span>
        </div>
        <div class="pricing-inner">
            <div class="pricing-slider">
                <div>
                    <div class="pricing">
                        <div class="header">
                            <div class="title">
                                <h3>
                                    STARTER
                                </h3>
                            </div>
                            <div class="price">
                                <div class="circle">
                                    <div class="text">
                                        <span>$19</span><br>per month
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="body">
                            <div class="info">
                                <p>Competition Analysis Methods</p>
                                <p>All Ranked URLs</p>
                                <p>International Support System</p>
                                <p>Social Media Tracking</p>
                            </div>
                            <div class="button-group">
                                <a href="#" class="c-btn c-btn-md">CHOOSE PLAN</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div>
                    <div class="pricing">
                        <div class="header">
                            <div class="title">
                                <h3>
                                    STARTER
                                </h3>
                            </div>
                            <div class="price">
                                <div class="circle">
                                    <div class="text">
                                        <span>$19</span><br>per month
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="body">
                            <div class="info">
                                <p>Competition Analysis Methods</p>
                                <p>All Ranked URLs</p>
                                <p>International Support System</p>
                                <p>Social Media Tracking</p>
                            </div>
                            <div class="button-group">
                                <a href="#" class="c-btn c-btn-md">CHOOSE PLAN</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div>
                    <div class="pricing">
                        <div class="header">
                            <div class="title">
                                <h3>
                                    STARTER
                                </h3>
                            </div>
                            <div class="price">
                                <div class="circle">
                                    <div class="text">
                                        <span>$19</span><br>per month
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="body">
                            <div class="info">
                                <p>Competition Analysis Methods</p>
                                <p>All Ranked URLs</p>
                                <p>International Support System</p>
                                <p>Social Media Tracking</p>
                            </div>
                            <div class="button-group">
                                <a href="#" class="c-btn c-btn-md">CHOOSE PLAN</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>