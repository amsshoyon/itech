<footer class="footer-section">
    <div class="footer-top overlay" style="background-image:url('assets/images/bg/bg2.jpg');">
        <div class="container">
            <div class="inner-section">
                <div class="title">
                    <h2>
                        Let's Get Started Now. <span> It's FREE!</span>
                    </h2>
                </div>
                <div class="info">
                    <h4>
                        30 day free trial. Free plan allows up to 2 projects. Pay if you need more. Cancel anytime. No
                        catches.
                    </h4>
                </div>
                <div class="button-group">
                    <a href="#" class="c-btn c-btn-lg c-primary-btn">start free trial</a>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-main">
        <div class="container">
            <div class="inner-section">
                <div class="social">
                    <ul>
                        <li>
                            <a href="#">
                                <i class="fa fa-facebook"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-twitter"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-rss"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-google-plus"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-linkedin"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-skype"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-vimeo"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-tumblr"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="copyright">
                    <p>
                        Kazi Erfan © All Rights Reserved
                    </p>
                </div>
            </div>
        </div>
    </div>
</footer>

<a href="#" id="go-top">
    <i class="fa fa-angle-up"></i>
</a>