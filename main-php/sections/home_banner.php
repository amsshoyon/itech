<section id="home-slider" class="home-slider carousel slide carousel-fade" data-ride="carousel" data-interval="false">
    <ul class="carousel-indicators">
        <li data-target="#home-slider" data-slide-to="0" class="active"></li>
        <li data-target="#home-slider" data-slide-to="1"></li>
        <li data-target="#home-slider" data-slide-to="2"></li>
    </ul>
    <div class="carousel-inner">
        <div class="carousel-item active">
            <div class="carousel-image">
                <img src="assets/images/slider/home/header2.jpg" alt="carousel 1">
            </div>

            <div class="carousel-caption">
                <div class="caption-inner">
                    <div class="title-top animated fadeInDown delay-1s">
                        <h3>Our Clients Are Our First Priority</h3>
                    </div>
                    <div class="title animated zoomIn slow">
                        <h1>Welcome To <span>i-Tech</span></h1>
                    </div>
                    <div class="horizontal-line animated fadeInUp slow"></div>
                    <div class="info animated fadeInUp slow">
                        <h5>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                            been the industry's standard dummy text ever since the 1500s, when an unknown printer took a
                            galley of type and scrambled it to make a type specimen book.
                        </h5>
                    </div>
                    <div class="button-group animated fadeInUp slow">
                        <a href="#" class="c-btn c-btn-lg c-primary-btn">GET STARTED NOW</a>
                        <a href="#" class="c-btn c-btn-lg c-trans-btn">Learn More</a>
                    </div>
                </div>
                <div class="darker-bg"></div>
            </div>
        </div>
        <div class="carousel-item">
            <div class="carousel-image">
                <img src="assets/images/slider/home/header3.jpg" alt="carousel 1">
            </div>

            <div class="carousel-caption">
                <div class="caption-inner">
                    <div class="title-top animated fadeInDown delay-1s">
                        <h3>Elegant, Modern and fully customizable saas and webapp template</h3>
                    </div>
                    <div class="title animated zoomIn slow">
                        <h1>Smart - App Landing Pages</h1>
                    </div>
                    <div class="horizontal-line animated fadeInUp slow"></div>
                    <div class="info animated fadeInUp slow">
                        <h5>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                            been the industry's standard dummy text ever since the 1500s, when an unknown printer took a
                            galley of type and scrambled it to make a type specimen book.
                        </h5>
                    </div>
                    <div class="button-group animated fadeInUp slow">
                        <a href="#" class="c-btn c-btn-lg c-primary-btn">GET STARTED NOW</a>
                        <a href="#" class="c-btn c-btn-lg c-trans-btn">Learn More</a>
                    </div>
                </div>
                <div class="darker-bg"></div>
            </div>
        </div>
        <div class="carousel-item">
            <div class="carousel-image">
                <img src="assets/images/slider/home/header1.jpg" alt="carousel 1">
            </div>

            <div class="carousel-caption">
                <div class="caption-inner">
                    <div class="title-top animated fadeInDown delay-1s">
                        <h3>Innovative core with endless possibilities.</h3>
                    </div>
                    <div class="title animated zoomIn slow">
                        <h1>Ultra modern design</h1>
                    </div>
                    <div class="horizontal-line animated fadeInUp slow"></div>
                    <div class="info animated fadeInUp slow">
                        <h5>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                            been the industry's standard dummy text ever since the 1500s, when an unknown printer took a
                            galley of type and scrambled it to make a type specimen book.
                        </h5>
                    </div>
                    <div class="button-group animated fadeInUp slow">
                        <a href="#" class="c-btn c-btn-lg c-primary-btn">GET STARTED NOW</a>
                        <a href="#" class="c-btn c-btn-lg c-trans-btn">Learn More</a>
                    </div>
                </div>
                <div class="darker-bg"></div>
            </div>
        </div>
    </div>
    <a class="carousel-btn carousel-control-prev" href="#home-slider" data-slide="prev">
        <span class="fa fa-angle-left"></span>
    </a>
    <a class="carousel-btn carousel-control-next" href="#home-slider" data-slide="next">
        <span class="fa fa-angle-right"></span>
    </a>
    <div class="go-down">
        <a href="#features" class="circle">
            <img src="assets/images/icons/ancor.svg" alt="Go Down">
        </a>
    </div>

</section>