<section class="client-section">
    <div class="container">
        <div class="title-section">
            <div class="sec-title">
                <h2>
                    Great Integrations with Others
                </h2>
            </div>
            <div class="info">
                <p>
                    Suspendisse sed eros mollis, tincidunt felis eget, interdum erat.
                    Nullam sit amet odio eu est aliquet euismod a a urna. Proin eu urna suscipit, dictum quam nec.
                </p>
            </div>
            <span class="horizontal-line alt"></span>
        </div>
        <div class="client-inner">
            <div class="client-slider">
                <div>
                    <div class="client">
                        <div class="image">
                            <img src="assets/images/slider/clients/client1.png" alt="client">
                        </div>
                    </div>
                </div>
                <div>
                    <div class="client">
                        <div class="image">
                            <img src="assets/images/slider/clients/client2.png" alt="client">
                        </div>
                    </div>
                </div>
                <div>
                    <div class="client">
                        <div class="image">
                            <img src="assets/images/slider/clients/client3.png" alt="client">
                        </div>
                    </div>
                </div>
                <div>
                    <div class="client">
                        <div class="image">
                            <img src="assets/images/slider/clients/client4.png" alt="client">
                        </div>
                    </div>
                </div>
                <div>
                    <div class="client">
                        <div class="image">
                            <img src="assets/images/slider/clients/client5.png" alt="client">
                        </div>
                    </div>
                </div>
                <div>
                    <div class="client">
                        <div class="image">
                            <img src="assets/images/slider/clients/client6.png" alt="client">
                        </div>
                    </div>
                </div>
                <div>
                    <div class="client">
                        <div class="image">
                            <img src="assets/images/slider/clients/client7.png" alt="client">
                        </div>
                    </div>
                </div>
                <div>
                    <div class="client">
                        <div class="image">
                            <img src="assets/images/slider/clients/client8.png" alt="client">
                        </div>
                    </div>
                </div>
                <div>
                    <div class="client">
                        <div class="image">
                            <img src="assets/images/slider/clients/client9.png" alt="client">
                        </div>
                    </div>
                </div>
                <div>
                    <div class="client">
                        <div class="image">
                            <img src="assets/images/slider/clients/client10.png" alt="client">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>